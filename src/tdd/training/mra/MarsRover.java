package tdd.training.mra;

import java.util.List;

public class MarsRover {

	private Integer[][] pianeta;
	private String facing;
	private String coordObstacleHit;
	private int roverStatusX;
	private int roverStatusY;
	private int roverStatusXBackMove;
	private int roverStatusYBackMove;
	

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		pianeta = new Integer[planetX][planetY];
		roverStatusX = 0;
		roverStatusY = 0;
		pianeta[roverStatusX][roverStatusY] = 1;
		facing = "N";
		for(int j = 0; j<planetY; j++)
		{
			for(int z = 0; z<planetX; z++)
			{
				pianeta[z][j] = 0;
			}
		}
		
		for(int i = 0; i<planetObstacles.size(); i++)
		{
			String temp = planetObstacles.get(i);
			temp = temp.replace("(", "");
			temp = temp.replace(")", "");
			
			String[] splitted = temp.split(",");
			int obstacleAtX = Integer.parseInt(splitted[0]);
			int obstacleAtY = Integer.parseInt(splitted[1]);
			
			pianeta[obstacleAtX][obstacleAtY] = -1;
		}
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
		return pianeta[x][y] == -1;
		
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
	
		int flag = 0;
		if(commandString != "")
		{
			String singleCommand;
			
			
			for(int i = 0; i<commandString.length(); i++)
			{
				
				singleCommand = String.valueOf(commandString.charAt(i));
				
				if(facing.equals("N"))
				{
					if(roverStatusX == 0 && roverStatusY==0 && singleCommand.equals("b"))
					{
						roverStatusY = 9;

					}
					else if(singleCommand.equals("l"))
					{
						facing = "W";
						
					}
					else if(singleCommand.equals("f"))
					{
						//backup last pos
						roverStatusXBackMove = roverStatusX;
						roverStatusYBackMove = roverStatusY;
						//setlastpos to 0
						pianeta[roverStatusX][roverStatusY] = 0;
						//setnewcoord
						roverStatusY++;
						//check for obstacle
						if(pianeta[roverStatusX][roverStatusY] == -1)
						{
							flag = 1;
							pianeta[roverStatusXBackMove][roverStatusYBackMove] = 1;
							coordObstacleHit = "("+roverStatusX+","+roverStatusY+")";
							System.out.println(coordObstacleHit);
						}
						else
						{
							
							pianeta[roverStatusX][roverStatusY] = 1;
						}
						
					}
					else if(singleCommand.equals("b") && roverStatusY > 0)
					{
						pianeta[roverStatusX][roverStatusY] = 0;
						roverStatusY--;
						pianeta[roverStatusX][roverStatusY] = 1;
						
					}
					else if(singleCommand.equals("r"))
					{
						facing = "E";	
					}
				}
				else if(singleCommand.equals("f")  && facing.equals("E"))
				{
					pianeta[roverStatusX][roverStatusY] = 0;
					roverStatusX++;
					pianeta[roverStatusX][roverStatusY] = 1;
				}
			}
			
		}
		
		if(flag == 1)
		{
			System.out.println("A");
			return "(" + roverStatusX + ","+ roverStatusY+","+facing+")" + coordObstacleHit;
		}
		else
		{
			return "(" + roverStatusX + ","+ roverStatusY+","+facing+")";
		}
		
	
		
		
	}
	
	
	

}
