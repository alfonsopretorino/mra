package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testMarsRover() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(4,7)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		assertEquals(true, roverTest.planetContainsObstacleAt(4, 7));
		
	}
	
	@Test
	public void testExecuteCommand() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(4,7)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		
		assertEquals("(0,0,N)", roverTest.executeCommand(""));
		
	}
	@Test
	public void testExecuteCommandTuringRight() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(4,7)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		
		assertEquals("(0,0,E)", roverTest.executeCommand("r"));
		
	}
	
	@Test
	public void testExecuteCommandTuringLeft() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(4,7)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		
		assertEquals("(0,0,W)", roverTest.executeCommand("l"));
		
	}
	
	@Test
	public void testExecuteCommandMovingForward() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(4,7)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		assertEquals("(0,1,N)", roverTest.executeCommand("f"));
		
	}
	
	@Test
	public void testExecuteCommandMovingBackward() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(5,7)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		roverTest.executeCommand("f");
		assertEquals("(0,0,N)", roverTest.executeCommand("b"));
		
	}
	
	@Test
	public void testExecuteCommandMovingCombined() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(5,7)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		
		assertEquals("(2,2,E)", roverTest.executeCommand("ffrff"));
		
	}
	
	@Test
	public void testExecuteCommandMovingBeyondTheEdge() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(5,7)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		assertEquals("(0,9,N)", roverTest.executeCommand("b"));
		
	}
	
	@Test
	public void testSingleObstacleEncounter() throws MarsRoverException {
		
		List<String> ostacoliTest = new ArrayList<>();
		ostacoliTest.add("(2,2)");
		MarsRover roverTest = new MarsRover(10, 10, ostacoliTest);
		assertEquals("(1,2,E)(2,2)", roverTest.executeCommand("ffrff"));
		
	}
	
	


}
